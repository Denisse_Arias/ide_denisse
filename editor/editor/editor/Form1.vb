﻿Public Class IDE
   

    Private pes(5) As Windows.Forms.TabPage
    Private lineas As Integer = 0, cuadro1 As Integer = 0, lorenz As Integer = 0
    Private nombre_archivo As String
    Private cuadro(50) As Windows.Forms.TextBox

    Private Sub NuevoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem.Click

        If cuadro1 < 5 And lorenz < 5 Then  'ciclo para contar las ventanas que podemos abrir
            pes(lorenz) = New Windows.Forms.TabPage 'creamos los tabcontrol en la posicion del contador lorenz 
            pes(lorenz).Text = "Doc " & (cuadro1 + 1) 'le ponemos el nombre a las pestañas
            pestanas.TabPages.Add(pes(lorenz))
            cuadro(cuadro1) = New Windows.Forms.TextBox  'Aqui cree los textbox dentro de los tabcontrol
            cuadro(cuadro1).Multiline = True
            pestanas.SelectedIndex = pestanas.TabPages.Count - 1
            pestanas.SelectedTab.Controls.Add(cuadro(cuadro1))
            cuadro(cuadro1).Dock = DockStyle.Fill
            cuadro(cuadro1).Parent = pestanas.SelectedTab
            cuadro(cuadro1).Select()
            cuadro1 += 1
        Else
            MessageBox.Show("No puedes crear mas de 5 pestañas", "ERROR")
        End If

    End Sub
    Private Sub AbrirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AbrirToolStripMenuItem.Click

        Dim dir_ruta As String
        Dim tab_pestañas As Windows.Forms.TabControl
        Dim tiboi As Windows.Forms.RichTextBoxSelectionAttribute
        Dim i As Integer
        Dim name As String = Nothing
        Dim inicio As Integer = abrirarchivos.FileName.LastIndexOf("\")
        Dim final As Integer = abrirarchivos.FileName.LastIndexOf(".")
        If abrirarchivos.ShowDialog = Windows.Forms.DialogResult.OK Then
            If i = 0 Then
                tab_pestañas = New Windows.Forms.TabControl
                Me.Controls.Add(tab_pestañas)
                tab_pestañas.Dock = DockStyle.Fill

                tab_pestañas.BringToFront()
                ' AddHandler tab_pestañas.SelectedIndexChanged, AddressOf tabcontrol_IndeiChanged

            End If

            For y As Integer = inicio + 1 To final - 1
                name += abrirarchivos.FileName.Chars(y)
            Next

            ReDim Preserve dir_ruta(i)
            dir_ruta(i) = abrirarchivos.FileName
            tab_pestañas.TabPages.Add(name)
            ReDim Preserve tiboi(i)
            tiboi(i) = New Windows.Forms.RichTextBox
            tab_pestañas.TabPages.Item(i).Controls.Add(tiboi(i))
            tiboi(i).Dock = DockStyle.Fill
            tab_pestañas.SelectedIndex = i
            ' AddHandler tiboi(i).TextChanged,',''AddressOf RichTextBoies_TeitChanged
            i += 1
            Dim stringr As New System.IO.StreamReader(abrirarchivos.FileName, FileMode.Open)
            While Not stringr.EndOfStream
                tiboi(tab_pestañas.SelectedIndex).Text += stringr.ReadLine + vbNewLine
            End While
            stringr.Close()



            If tab_pestañas.SelectedIndex >= 0 Then

                'contador de líneas'
                ' lineas = "Lineas: " & tiboi(tab_pestañas.SelectedIndex).Lines.Count.ToString
            End If


            'If openFiledlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            '    opennewtab()
            '    Dim stringr As New StreamReader(openFiledlg.FileName, FileMode.Open)
            '    While Not stringr.EndOfStream
            '        textos(tabcon.SelectedIndex).Text += stringr.ReadLine + vbNewLine
            '    End While
            '    stringr.Close()
            '    contador()
            'End If

            ''****

            ' abrirarchivos.ShowDialog()
            'Dim a As New System.IO.StreamReader(abrirarchivos.FileName)
            'cuadro(cuadro1).Text = a.ReadToEnd


            'If abrirarchivos.ShowDialog = Windows.Forms.DialogResult.OK Then


            '    Dim sr As New System.IO.StreamReader(abrirarchivos.FileName) 'variable que manda llamar la ventana de archivos y lea lo que hay dentro del archivo
            '    While Not sr.EndOfStream
            '        ActiveControl.Text = (sr.ReadToEnd) 'vamos a leer el archivo texto que seleccionemos del filename 
            '    End While

            '    sr.Close() ' Una vez seleccionado, se cierra la ventana de archivos


        End If





        'If abrirarchivos.ShowDialog() = Windows.Forms.DialogResult.OK Then
        '    Dim sr As New System.IO.StreamReader(abrirarchivos.FileName) 'variable que manda llamar la ventana de archivos

        '    While Not sr.EndOfStream
        '        cuadro(lorenz).Text = (sr.ReadToEnd) 'vamos a leer el archivo texto que seleccionemos del filename 
        '    End While
        '    sr.Close() ' Una vez seleccionado, se cierra la ventana de archivos

        'End If

    End Sub
    End Sub
    Private Sub CerrarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CerrarToolStripMenuItem.Click
        Dim current_tab As Windows.Forms.TabPage = pestanas.SelectedTab
        pestanas.TabPages.Remove(current_tab) 'Removemos todas las pestañas
    End Sub
    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close() 'cerramos el programa
    End Sub

    Private Sub IDE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cuadro(cuadro1) = New Windows.Forms.TextBox
        cuadro(cuadro1).Multiline = True
        pestanas.SelectedIndex = pestanas.TabPages.Count - 1 '
        pestanas.SelectedTab.Controls.Add(cuadro(cuadro1))
        cuadro(cuadro1).Dock = DockStyle.Fill 'el textbox se adapta al tamaño del form
        cuadro(cuadro1).Parent = pestanas.SelectedTab
        cuadro(cuadro1).Select()
        cuadro1 += 1

        TabPage1.Text = "Doc " & (pestanas.TabPages.Count) 'nombre de las pestañas 
    End Sub

    Private Sub GuardarComoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarComoToolStripMenuItem.Click
        Dim fichero As New System.IO.StreamWriter(guardar_como.FileName)
        guardar_como.Filter = "Archivos de texto (*.txt)|*.txt|Todos (*.*)|*.*" 'Solo para abrir los archovos de texto
        guardar_como.FilterIndex = 1 ' El indice 
        guardar_como.DefaultExt = "txt"
        guardar_como.FileName = nombre_archivo
        guardar_como.Title = "Guardar archivo como"
        guardar_como.ShowDialog()

        If guardar_como.FileName <> "" Then
            fichero.WriteLine(ActiveControl.Text)
            nombre_archivo = guardar_como.FileName 'guardar el nombre que pongamos en el savefile 
            TabPage1.Text = nombre_archivo 'la pestaña se llamara como le pusimos al nombre del archivo
        Else
            guardar_como.FileName = nombre_archivo ' guardamos el archivo cuando le pusimos ya un nombre
        End If
        fichero.Close()
    End Sub

    Private Sub ShellToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShellToolStripMenuItem.Click
        Process.Start("especiales.exe")
    End Sub
    Private Sub CopiarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopiarToolStripMenuItem.Click
        Clipboard.Clear() 'para limpiar el portapapeles
        Clipboard.SetText(cuadro(cuadro1 - 1).SelectedText) ' para obtener el texto seleccionado del docuemento que esta abierto
        cuadro(cuadro1 - 1).Focus()
    End Sub
    Private Sub PegarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PegarToolStripMenuItem.Click
        cuadro(cuadro1 - 1).SelectedText = Clipboard.GetText()
        cuadro(cuadro1 - 1).Focus()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        Process.Start("ver_ayuda.chm") 'inicia el proceso del archivo 
    End Sub

    Private Sub CortarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortarToolStripMenuItem.Click
        Clipboard.SetText(cuadro(cuadro1 - 1).SelectedText) 'seleccionamos el texto
        cuadro(cuadro1 - 1).SelectedText = "" 'ahora el textbox estará vacio ""
        cuadro(cuadro1 - 1).Focus()
    End Sub

    Private Sub InsertarHoraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InsertarHoraToolStripMenuItem.Click
        cuadro(cuadro1 - 1).Text = Format(TimeOfDay) ' con la funcion time, pongo la hora
    End Sub

    Private Sub InsertarFechaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InsertarFechaToolStripMenuItem.Click
        cuadro(cuadro1 - 1).Text = Format(Date.Today) 'inserte con la funcion date, la fecha 
    End Sub

    Private Sub AyudaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AyudaToolStripMenuItem1.Click
        Process.Start("ayuda.chm") 'mando llamar el interprete
    End Sub
End Class
